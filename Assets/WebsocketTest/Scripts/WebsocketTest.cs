﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using WebSocketSharp;
using System;

public class WebsocketTest : MonoBehaviour {

	public InputField inputField;

	public string url;
	private WebSocket ws;

	void Start () {
		Connect();
	}

	private void Connect() {
		ws = new WebSocket(url);

		ws.OnOpen += (sender, e) => {
			Debug.Log("Websocket Open");
		};
		ws.OnError += (sender, e) => {
			Debug.Log("Websocket Error " + e.Message);
		};
		ws.OnMessage += OnMessage;
		ws.OnClose += (sender, e) => {
			Debug.Log("Websocket Close ");
		};

		Debug.Log("Start Connect");
		ws.Connect();
		Debug.Log("End Connect");
	}

	public void OnClick() {
		Debug.Log(inputField.text);
		ws.Send(inputField.text);
		inputField.text = "";
	}

	private class Message {
		public string message;
	}

	private class MoveMessage {
		public int X;
		public int Y;
		public override string ToString ()
		{
			return string.Format ("X: {0}, Y: {1}", X, Y);
		}
	}

	private class ErrMessage {
		public string error;
		public override string ToString ()
		{
			return string.Format ("{0}", error);
		}
	}

	[Serializable]
	private class BanMessage {
		List<Koma> ban;
		List<Koma> tegoma;
	}

	[Serializable]
	private class Koma {
		int ID;
		int PlayerID;
		int Type;
		int X;
		int Y;
	}

	private void OnMessage(object sender, MessageEventArgs e) {
		if (!e.IsText) {
			return;
		}
		Debug.Log(e.Data);
		var m = JsonUtility.FromJson<Message>(e.Data);
		switch (m.message) {
		case "move":
			Debug.Log(JsonUtility.FromJson<MoveMessage>(e.Data));
			break;
		case "error":
			Debug.Log(JsonUtility.FromJson<ErrMessage>(e.Data));
			break;
		case "ban":
			Debug.Log(JsonUtility.ToJson(JsonUtility.FromJson<BanMessage>(e.Data)));
			break;
		default:
			break;
		}
	}
}
