﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour {

    [SerializeField] TweenPosition inputTweenPosition;
    [SerializeField] UIInput input;

    [SerializeField] GameObject windowRoot;
    [SerializeField] GameObject waitJoinWindowPrefab;
    [SerializeField] GameObject errorWindowPrefab;

    private int mode; // 0: default, 1: mini, 2: micro

    private bool initialized = false;
    private ErrorResponse errorResponse = null;

    private GameObject window;

    void OnEnable() 
    {
        NetworkManager.Instance.onReceiveErrorResponse += OnReceiveErrorResponse;
        NetworkManager.Instance.onReceiveJoinResponse += OnReceiveJoinResponse;
        NetworkManager.Instance.onReceiveInitializeResponse += OnReceiveInitializeResponse;
    }

    void OnDisable() 
    {
        NetworkManager.Instance.onReceiveErrorResponse -= OnReceiveErrorResponse;
        NetworkManager.Instance.onReceiveJoinResponse -= OnReceiveJoinResponse;
        NetworkManager.Instance.onReceiveInitializeResponse -= OnReceiveInitializeResponse;
    }

    void Start()
    {
        NetworkManager.Instance.Open();
        SoundManager.Instance.PlayBGM(SoundManager.TYPE.BGM_TITLE);
    }

    void Update() 
    {
        if (initialized) 
        {
            SceneManager.LoadScene("Main");
            initialized = false;
        }

        if (errorResponse != null)
        {
            if (errorResponse.msg == "too many player")
            {
                window = NGUITools.AddChild(windowRoot, errorWindowPrefab);
                window.GetComponentInChildren<UILabel>().text = "使用中です";
                window.GetComponentInChildren<UIButton>().onClick.Add(new EventDelegate(this, "OnClickWindow"));
            }
            errorResponse = null;
        }
    }

    public void OnClickDefaultMode()
    {
        mode = 0;
        inputTweenPosition.PlayForward();
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickMiniMode()
    {
        mode = 1;
        inputTweenPosition.PlayForward();
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickMicroMode()
    {
        mode = 2;
        inputTweenPosition.PlayForward();
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickBack()
    {
        inputTweenPosition.PlayReverse();
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickSendButton() 
    {
        window = NGUITools.AddChild(windowRoot, waitJoinWindowPrefab);
        window.GetComponentInChildren<UIButton>().onClick.Add(new EventDelegate(this, "OnClickWindowAndConnection"));

        var name = input.value;
        var joinRequest = new JoinRequest{ name = name, mode = mode };
        NetworkManager.Instance.Open();
        NetworkManager.Instance.Send<JoinRequest>(Constants.JoinRequestMessage, joinRequest);
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickRandomButton()
    {
        window = NGUITools.AddChild(windowRoot, waitJoinWindowPrefab);
        window.GetComponentInChildren<UIButton>().onClick.Add(new EventDelegate(this, "OnClickWindowAndConnection"));

        NetworkManager.Instance.Open();
        NetworkManager.Instance.Send<RandomJoinRequest>(Constants.RandomJoinRequestMessage, new RandomJoinRequest{ mode = mode });
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickWindow()
    {
        Destroy(window);
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnClickWindowAndConnection()
    {
        NetworkManager.Instance.Close();
        NetworkManager.Instance.Open();
        Destroy(window);
        SoundManager.Instance.PlaySE(SoundManager.TYPE.OK);
    }

    public void OnReceiveJoinResponse(JoinResponse data) 
    {
        Debug.LogWarning("(TitleScene) Receive JoinResponse");
        Debug.LogWarning("(TitleScene) player id : " + data.player.id.ToString());
        GameData.Instance.MyPlayer = data.player;
    }

    public void OnReceiveInitializeResponse(InitializeResponse data) 
    {
        Debug.LogWarning("(TitleScene) Receive InitializeResponse");
        GameData.Instance.InitializeResponse = data;
        initialized = true;
    }

    public void OnReceiveErrorResponse(ErrorResponse data) 
    {
        Debug.LogWarning("(TitleScene) Receive ErrorResponse");
        errorResponse = data;
    }
}
