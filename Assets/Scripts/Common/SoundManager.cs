﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : SingletonMonoBehaviour<SoundManager> {

	bool enable;
	public bool Enable {
		get {
			return enable;
		}
		set {
			enable = value;
			audioSource.mute = !enable;
		}
	}

	private Dictionary<TYPE, string> audioResource;
	private AudioSource audioSource;
	private Dictionary<TYPE, AudioClip> audioClipCache;

	public enum TYPE
	{
		OK,
		NG,
        READY,
        START,
        MOVABLE,
        MOVE,
        MOVE_AND_CAPTURED,
		CLEAR,
		BGM_TITLE,
		BGM_MAIN,
	}

	public void Awake()
	{
		if(this != Instance)
		{
			Destroy(this);
			return;
		}

		DontDestroyOnLoad(this.gameObject);

		audioResource = new Dictionary<TYPE, string> {
            // via http://www.kurage-kosho.info/others.html
			{TYPE.OK, "Sounds/clappers01"},
            {TYPE.NG, "Sounds/clappers01"},
            {TYPE.MOVABLE, "Sounds/wood-block01"},
            {TYPE.MOVE, "Sounds/hand-drum01"},
            {TYPE.MOVE_AND_CAPTURED, "Sounds/taiko01"},
            {TYPE.READY, "Sounds/taiko01"},
            {TYPE.START, "Sounds/taiko02"},
            {TYPE.CLEAR, "Sounds/taiko02"},
            // via http://www.hmix.net/music_gallery/image/asian.htm
			{TYPE.BGM_TITLE, "Sounds/TitleBGM"},
            {TYPE.BGM_MAIN, "Sounds/MainBGM"},
		};

		audioSource = gameObject.AddComponent<AudioSource> ();

		audioClipCache = new Dictionary<TYPE, AudioClip> ();

		// Enable = PlayerPrefs.GetInt (Const.PREF.SOUND_ENABLE.ToString (), 1) == 1 ? true : false;
	}

	public void ToggleActivate () {
		Enable = !Enable;
		// PlayerPrefs.SetInt (Const.PREF.SOUND_ENABLE.ToString (), Enable ? 1 : 0);
	}

    public void PlaySE (TYPE t, float volume = 1f) 
    {
//		if (!Enable)
//			return;

		AudioClip clip = GetAudioClip (t);

		audioSource.PlayOneShot (clip, volume);
	}
	public void PlayOk () {
		this.PlaySE (TYPE.OK);
	}

	public void PlayBGM (TYPE t) {
		audioSource.clip = GetAudioClip (t);
		audioSource.loop = true;
		audioSource.Play ();
	}

	AudioClip GetAudioClip (TYPE t) {
		AudioClip clip;
		if (!audioClipCache.ContainsKey (t)) {
			clip = (AudioClip)Resources.Load (audioResource[t]);
			audioClipCache [t] = clip;
		} else {
			clip = audioClipCache [t];
		}
		return clip;
	}

}
