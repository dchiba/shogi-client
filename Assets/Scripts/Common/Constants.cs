﻿using UnityEngine;
using System.Collections;

public class Constants {

    public const string RandomJoinRequestMessage = "random_join";
    public const string JoinRequestMessage = "join";
    public const string JoinResponseMessage = "join";
    public const string InitializeResponseMessage = "initialize";
    public const string InitializedRequestMessage = "initialized";
    public const string StartResponseMessage = "start";
    public const string MovablePositionsRequestMessage = "movable_positions";
    public const string MovablePositionsResponseMessage = "movable_positions";
    public const string MoveRequestMessage = "move";
    public const string MoveResponseMessage = "move";
    public const string PutKomaRequestMessage = "put_koma";
    public const string PutKomaResponseMessage = "put_koma";
    public const string FinishResponseMessage = "finish";
    public const string ErrorResponseMessage = "error";
    public const string LeaveResponseMessage = "leave";

    public const string FocusSpriteName = "shogi_focus";
    public const string SelectedFocusSpriteName = "shogi_focus_bold";

    public static float[][] KomaColors = new float[][]{
        new float[]{ 135f, 195f, 255f },
        new float[]{ 255f, 136f, 75f },
        new float[]{ 243f, 255f, 129f },
        new float[]{ 148f, 255f, 147f },
        new float[]{ 255f, 158f, 251f },
    };

    public static int[] UIRootWidth = new int[] {1000, 800, 800};

    public static float[][] CameraPositions = new float[][]{
        new float[] { 0, 0, 0 },
        new float[] { 0, -50f, 0 },
        new float[] { 0, -50f, 0 },
    };

    public static float[][] TegomaRoot1Positions = new float[][]{
        new float[] { -350f, -500f, 0 },
        new float[] { -175f, -400f, 0 },
        new float[] { -88f, -300f, 0 },
    };

    public static float[][] TegomaRoot2Positions = new float[][]{
        new float[] { 350f, 500f, 0 },
        new float[] { 175f, 300f, 0 },
        new float[] { 88f, 200f, 0 },
    };
}
