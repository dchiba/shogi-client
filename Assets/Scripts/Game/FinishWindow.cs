﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinishWindow : MonoBehaviour 
{
    [SerializeField] UISprite sprite;
    [SerializeField] UILabel descriptionLabel;

    public void SetFinishResponse(FinishResponse data)
    {
        var finishResponse = data;

        var gameData = GameData.Instance;

        if (gameData.MyPlayer.id == finishResponse.winner.id)
        {
            sprite.spriteName = "shogi_WIN";
        }
        else
        {
            sprite.spriteName = "shogi_LOSE";
        }
    }

    public void SetLeaveResponse(LeaveResponse data)
    {
        sprite.spriteName = "shogi_WIN";
        descriptionLabel.text = "相手が退室しました";
    }

    public void OnClick()
    {
        NetworkManager.Instance.Close();

        SceneManager.LoadScene("Title");
    }
}
