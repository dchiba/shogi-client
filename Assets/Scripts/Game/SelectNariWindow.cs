﻿using UnityEngine;
using System.Collections;

public class SelectNariWindow : MonoBehaviour 
{
    public FocusModel focusModel { get; set; }

    public event System.Action<FocusModel, bool> onClickAction;

    public void OnClickNari()
    {
        OnClickButton(true);
    }

    public void OnClickNarazu()
    {
        OnClickButton(false);
    }

    private void OnClickButton(bool nari)
    {
        if (onClickAction != null)
        {
            onClickAction(focusModel, nari);
        }

        Destroy(gameObject);
    }

    public void OnClickClose()
    {
        Destroy(gameObject);
    }
}
