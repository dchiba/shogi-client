﻿using UnityEngine;
using System.Collections;

public class TegomaModel : MonoBehaviour {

    public event System.Action<TegomaModel> onClickAction;

    public KomaBaseModel KomaBaseModel { get; set; }

    void OnClick()
    {
        if (onClickAction != null) {
            onClickAction(this);
        }
    }
}
