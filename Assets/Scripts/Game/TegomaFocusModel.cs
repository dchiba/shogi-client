﻿using UnityEngine;
using System.Collections;

public class TegomaFocusModel : MonoBehaviour {

    [SerializeField] UISprite sprite;

    public PositionInfo PositionInfo { get; set; }
    public string spriteName { get { return sprite.spriteName; } set { sprite.spriteName = value; } }

    public event System.Action<TegomaFocusModel> onClickAction;

    public void OnClick()
    {
        if (onClickAction != null)
        {
            onClickAction(this);
        }
    }
}
