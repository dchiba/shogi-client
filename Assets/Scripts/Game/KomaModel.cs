﻿using UnityEngine;
using System.Collections;

public class KomaModel : MonoBehaviour 
{
    [SerializeField] KomaBaseModel komaBaseModel;
    [SerializeField] UISprite gaugeSprite;
    [SerializeField] Animation gaugeFullAnimation;
    [SerializeField] TweenPosition tweenPosition;

    public event System.Action<KomaModel> onClickAction;

    private KomaInfo komaInfo;
    public KomaInfo KomaInfo 
    {
        get
        {
            return komaInfo;
        }
        set
        {
            SetKomaInfo(value);
        }
    }
    public bool Movable { get; private set; }
    public bool Moving { get; private set; }

    private GameData gameData;

    public void SetKomaInfo(KomaInfo info) 
    {
        komaInfo = info;
        gameData = GameData.Instance;
        komaBaseModel.SetKomaBaseInfo(info.koma_base);
    }

    void OnEnable ()
    {
        Movable = false;
        Moving = false;
    }

    void OnDisable ()
    {
    }

	void Update () 
    {
        if (komaInfo != null && !Movable)
        {
            gaugeSprite.fillAmount = MoveWaitRate();
            if (gaugeSprite.fillAmount <= 0) 
            {
                Movable = true;
                gaugeFullAnimation.Play("GaugeFull");
                if (gameData.IsPlaying()) {
                    SoundManager.Instance.PlaySE(SoundManager.TYPE.MOVABLE, 2f);
                }
            }
        }
	}

    float MoveWaitRate() 
    {
        var last = komaInfo.last_moved_time;
        var movable = komaInfo.movable_time;
        var rate = (float)(gameData.CurrentServerUnixtime() - last) / (float)(movable - last);
        return 1f - Mathf.Clamp01(rate);
    }

    void OnClick()
    {
        if (Movable && !Moving && onClickAction != null) {
            onClickAction(this);
        }
    }

    public void Move(Vector3 from, Vector3 to)
    {
        tweenPosition.from = from;
        tweenPosition.to = to;
        tweenPosition.ResetToBeginning();
        tweenPosition.enabled = true;
        tweenPosition.PlayForward();
        Moving = true;
    }

    public void OnFinishedMove()
    {
        Moving = false;
        Movable = false;
    }
}
