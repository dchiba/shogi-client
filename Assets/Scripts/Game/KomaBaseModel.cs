﻿using UnityEngine;
using System.Collections;

public class KomaBaseModel : MonoBehaviour {

    [SerializeField] UISprite sprite;

    public KomaBaseInfo komaBaseInfo { get; private set; }

    public void SetKomaBaseInfo(KomaBaseInfo info) 
    {
        komaBaseInfo = info;
        var spriteNames = new string[]{
            "", "ou", "hi", "kaku", "kin", "gin", "kei", "kyo", "fu",
            "ryu", "uma", "ngin", "nkei", "nkyo", "to",
            "hi", "kaku"
        };
        var isSente = GameData.Instance.SentePlayer.id == info.player.id;
        var spriteName = "shogi_" + spriteNames[komaBaseInfo.koma_type];
        sprite.spriteName = spriteName;

        if (!isSente)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 180f);
            sprite.color = GameData.Instance.Player1Color;
        }
        else
        {
            sprite.color = GameData.Instance.Player2Color;
        }
    }
}
